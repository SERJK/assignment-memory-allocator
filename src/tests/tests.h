#ifndef _TESTS_H
#define _TESTS_H

#include <stddef.h>
#include <stdio.h>

enum tests_result {
    TEST_FAILED,
    TEST_SUCCESSFUL
};

void print_result(enum tests_result result, char* numb);

enum tests_result Heap_allocation_test(void);

enum tests_result Normal_memory_allocation_test(size_t query1, size_t query2, size_t query3);

enum tests_result Block_memory_release_test(void);

enum tests_result Two_blocks_memory_release_test(void);

enum tests_result Memory_region_expansion_test(size_t query);

enum tests_result Memory_far_region_expansion_test(void);

#endif
