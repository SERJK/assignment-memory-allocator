#include "tests.h"
#include "../files/mem_internals.h"
#include "../files/mem.h"
#include "../files/util.h"


void print_result(enum tests_result result, char* numb) {
    char* message;
    switch (result) {
        case TEST_SUCCESSFUL:
            message = (char*) "SUCCESSFUL";
            break;
        case TEST_FAILED:
            message = (char*) "FAILED";
            break;
    }
    printf("\033[35m[TEST %s %s]\033[35m\n\033[0m\n", numb, message);
}

enum tests_result Heap_allocation_test(void) {
    void* heap = heap_init(REGION_MIN_SIZE);
    enum tests_result result;
    if (heap != NULL) {
        result = TEST_SUCCESSFUL;
    } else {
        result = TEST_FAILED;
    }
    debug_heap(stdout, HEAP_START);
    return result;
}

enum tests_result Normal_memory_allocation_test(size_t query1, size_t query2, size_t query3) {
    enum tests_result result;

    void* a = _malloc(query1);
    void* b = _malloc(query2);
    void* c = _malloc(query3);

    query1 = max(query1, 24);
    query2 = max(query2, 24);
    query3 = max(query3, 24);

    if ((block_get_header(a)->capacity.bytes == query1) && (block_get_header(b)->capacity.bytes == query2) && (block_get_header(c)->capacity.bytes == query3)) {
        result = TEST_SUCCESSFUL;
    } else {
        result = TEST_FAILED;
    }
    debug_heap(stdout, HEAP_START);

    _free(c);
    _free(b);
    _free(a);

    return result;
}

enum tests_result Block_memory_release_test(void) {
    enum tests_result result;

    void* a = _malloc(100);
    void* b = _malloc(100);
    void* c = _malloc(100);

    _free(c);
    debug_heap(stdout, HEAP_START);

    if ((block_get_header(c)->is_free == true) && (block_get_header(b)->capacity.bytes == 100) && (block_get_header(a)->capacity.bytes == 100)) {
        result = TEST_SUCCESSFUL;
    } else {
        result = TEST_FAILED;
    }

    _free(b);
    _free(a);

    return result;
}

enum tests_result Two_blocks_memory_release_test(void) {
    enum tests_result result;

    void* a = _malloc(100);
    void* b = _malloc(100);
    void* c = _malloc(100);

    _free(c);
    _free(b);
    debug_heap(stdout, HEAP_START);

    if ((block_get_header(c)->is_free == true) && (block_get_header(b)->is_free == true) && (block_get_header(a)->capacity.bytes == 100)) {
        result = TEST_SUCCESSFUL;
    } else {
        result = TEST_FAILED;
    }

    _free(a);

    return result;
}

enum tests_result Memory_region_expansion_test(size_t query) {
    enum tests_result result;
    void* a = _malloc(query);

    if (block_get_header(a)->capacity.bytes == query) {
        result = TEST_SUCCESSFUL;
    } else {
        result = TEST_FAILED;
    }

    debug_heap(stdout, HEAP_START);
    _free(a);

    return result;
}

enum tests_result Memory_far_region_expansion_test(void) {
    enum tests_result result;

    struct block_header* b = HEAP_START;
    for (struct block_header* i = b; i; i=i->next) {
        b = i;
    }

    alloc_region(b+ size_from_capacity(b->capacity).bytes, REGION_MIN_SIZE);
    void* a = _malloc(6000);

    if (block_get_header(a)->capacity.bytes == 6000) {
        result = TEST_SUCCESSFUL;
    } else {
        result = TEST_FAILED;
    }

    debug_heap(stdout, HEAP_START);
    _free(a);

    return result;
}
