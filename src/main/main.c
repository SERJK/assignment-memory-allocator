#include "../tests/tests.h"
#include <inttypes.h>
#include "files/mem_internals.h"

int main(void) {
    enum tests_result testsResult_0 = Heap_allocation_test();
    print_result(testsResult_0, "0");

    enum tests_result testsResult_1 = Normal_memory_allocation_test(sizeof(char[15]), 1700, 5*sizeof(uint64_t));
    print_result(testsResult_1, "1");

    enum tests_result testsResult_2 = Block_memory_release_test();
    print_result(testsResult_2, "2");

    enum tests_result testsResult_3 = Two_blocks_memory_release_test();
    print_result(testsResult_3, "3");

    enum tests_result testsResult_4 = Memory_region_expansion_test(REGION_MIN_SIZE-16);
    print_result(testsResult_4, "4");

    enum tests_result testsResult_5 = Memory_far_region_expansion_test();
    print_result(testsResult_5, "5");

    return 0;
}
