#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  Allocate a memory region and initialize it with a block */
struct region alloc_region (void const * addr, size_t query) {
    size_t actualSize = region_actual_size(query);
    struct region new_region = {
            .addr = map_pages(addr, actualSize, MAP_FIXED_NOREPLACE),
            .size = actualSize,
            .extends = false
    };
    if (new_region.addr == MAP_FAILED) {
        new_region.addr = map_pages(addr, actualSize, 0);
        if (new_region.addr == MAP_FAILED) {
            return REGION_INVALID;
        }
    }
    block_init(new_region.addr, (block_size) {new_region.size}, NULL);
    return new_region;
}

static void* block_after(struct block_header const* block);

void* heap_init( size_t initial ) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region)) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  Splitting blocks if the found free block is too big */
static bool block_splittable(struct block_header* restrict block, size_t query) {
  return block->is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    bool splitting = true;
    if (block_splittable(block, query) == false) { splitting = false; }
    else {
        if (query < BLOCK_MIN_CAPACITY) { query = BLOCK_MIN_CAPACITY; }

        block_size newBlockSize = (block_size) {block->capacity.bytes - query};
        void* newAddr = block->contents + query;

        block_init(newAddr, newBlockSize, block->next);
        block->next = (struct block_header*) (block->contents + query);
        block->capacity = (block_capacity) {query};
    }
    return splitting;
}


/* Merge adjacent free blocks */
static void* block_after(struct block_header const* block) {
  return (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd) {
  return (void*)snd == block_after(fst);
}
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next(struct block_header* block) {
    bool merging = true;
    if (block->next == NULL) {
        merging = false;
    } else {
        if (mergeable(block, block->next) == false) {
            merging = false;
        }
        else {
            block_capacity newCapacity = (block_capacity) {block->capacity.bytes + size_from_capacity(block->next->capacity).bytes};

            block->next = block->next->next;
            block->capacity = newCapacity;
        }
    }
    return merging;
}


/* If there is enough heap size */
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    struct block_search_result result = {BSR_REACHED_END_NOT_FOUND, block};

    if (block == NULL) {
        result.type = BSR_CORRUPTED;
        result.block = block;
    }

    while (block->next != NULL) {
        while (try_merge_with_next(block));
        if ((block->capacity.bytes >= sz) && (block->is_free)) {     //l
            result.block = block;
            result.type = BSR_FOUND_GOOD_BLOCK;
            break;
        }
        block = block->next;
    }

    if ((block_is_big_enough(sz, block)) && (block->is_free)) {
        result.block = block;
        result.type = BSR_FOUND_GOOD_BLOCK;
    } else {
        result.block = block;
    }
    return result;
}

/*  Try to allocate memory on the heap starting from the block `block` without trying to expand the heap */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    struct block_search_result good_block = find_good_or_last(block, query);
    if (good_block.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(good_block.block, query);
        good_block.block->is_free = false;
    }
    return good_block;
}

static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    struct region new_region = alloc_region(block_after(last), size_from_capacity((block_capacity) {query}).bytes);
    last->next = (struct block_header *) new_region.addr;
    return last->next;
}

/*  Implements the main logic of malloc and returns the header of the allocated block */
static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    struct block_search_result block = try_memalloc_existing(query, heap_start);
    if (block.type == BSR_FOUND_GOOD_BLOCK) {
        return block.block;
    } else {
        struct block_header* grow = grow_heap(block.block, query);
        if (grow != NULL) {
            block = try_memalloc_existing(query, block.block);
            return block.block;
        } else {
            return NULL;
        }
    }
}

void* _malloc(size_t query) {
  struct block_header* const addr = memalloc(query, (struct block_header*) HEAP_START);
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
  if (!mem) return ;
  struct block_header* header = block_get_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header));
}
